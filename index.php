<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * Verify Acreditacion block
 * --------------------------
 * Verify Acreditacion based on the unique codes displayed on issued acreditacions.
 * Full details of the issued Acreditacion is displayed including profile picture.
 * Mostly cosmetic changes to the original codes from Jean-Michel Védrine.
 * Original Autor & Copyright - Jean-Michel Védrine | 2014
 *
 * @copyright           2015 onwards Manieer Chhettri | Marie Curie, UK | <manieer@gmail.com>
 * @author              Augusto Villa | cunix.net
 * @package             block_verificador_acreditaciones 
 * @license             http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_login();
//require_once($CFG->dirroot . '/mod/acreditacion/lib.php');
//require_once($CFG->dirroot.'/mod/acreditacion/locallib.php');

// $record = new stdClass();
// $record->rutcode = '39953567';
// $record->nombre = 'Ismael';
// $record->apellido = 'Gonzalez';
// $record->accredited = 1;
// // $record->fecha_acreditacion = '2021-03-25';
// $record->perfil_acreditado = 'Abogado';
// $record->id = $DB->insert_record('acreditaciones', $record);

$id = required_param('rutnumber', PARAM_ALPHANUM);   // acreditacion code to verify.

$PAGE->set_pagelayout('standard');
$strverify = get_string('verifyacreditacion', 'block_verificador_acreditaciones');
$PAGE->set_url('/blocks/verificador_acreditaciones/index.php', array('rutnumber' => $id));
$context = context_system::instance();
$PAGE->set_context($context);

// Print the header.
$PAGE->navbar->add($strverify);
$PAGE->set_title($strverify);
$PAGE->set_heading($strverify);
$PAGE->requires->css('/blocks/verificador_acreditaciones/printstyle.css');
$PAGE->requires->css('/blocks/verificador_acreditaciones/styles.css');
echo $OUTPUT->header();

// var_dump($OUTPUT)

$ufields = user_picture::fields('u');

$sql = "SELECT ci.*
         FROM {acreditaciones} ci
         WHERE ci.rutcode = ?";
$acreditacions = $DB->get_records_sql($sql, array($id));

if (!$acreditacions) {
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo '<div id="block_verify_acreditacion"><br>';
    echo '<p class="notVerified">' . get_string('acreditacion', 'block_verificador_acreditaciones')
        . ' "' . $id . '" ' . '</p>';
    echo '<div class="wrapper-box">';
    echo '<div class="left"><br>' . get_string('notfound', 'block_verificador_acreditaciones') . '</div>';
    echo '<div><img src="pix/certnotverified.png" border="0" align="center"></div>';
    echo '</div></div>';
    echo $OUTPUT->box_end();
} else {
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo '<div id="block_verify_acreditacion"><br>';
    foreach ($acreditacions as $certdata) {

        if ($certdata->accredited == 0) {
            echo '<p class="notVerified">' . get_string('acreditacion', 'block_verificador_acreditaciones')
                . ' "' . $id . '" ' . '</p>';
            echo '<div class="left wrapper-box">';
            echo '<p class="title">' . get_string('check_fail', 'block_verificador_acreditaciones') . '</p>';
            echo '<p class="title">' . get_string('dates', 'block_verificador_acreditaciones') . '</p>';
            echo '<div class="margin-left">';
            echo '<p>Rutcode: ' . $certdata->rutcode . '<p>';
            echo '<p>Nombre: ' . $certdata->nombre . '<p>';
            echo '<p>Apellido: ' . $certdata->apellido . '<p>';
            // echo '<li>Fecha de Acreditación:'. $certdata->fecha_acreditacion .'<p>';
            echo '<p>Perfil Acreditado: ' . $certdata->perfil_acreditado . '<p>';
            echo '</div>';
            echo '</div>';
            echo '<div class="center"><img src="pix/certnotverified.png" border="0" align="center">';
            echo '<p class="notVerified">' . get_string('check_fail_state', 'block_verificador_acreditaciones') . '</p>';
            echo '</div>';
            echo '</div>';
        } else {
            echo '<p class="verified">' . get_string('acreditacion', 'block_verificador_acreditaciones')
                . ' "' . $id . '" ' . '</p>';
            echo '<div class="left wrapper-box">';
            echo '<p class="title">' . get_string('check', 'block_verificador_acreditaciones') . '</p>';
            echo '<p class="title">' . get_string('dates', 'block_verificador_acreditaciones') . '</p>';
            echo '<div class="margin-left">';
            echo '<p>Rutcode: ' . $certdata->rutcode . '<p>';
            echo '<p>Nombre: ' . $certdata->nombre . '<p>';
            echo '<p>Apellido: ' . $certdata->apellido . '<p>';
            // echo '<p>Fecha de Acreditación:'. $certdata->fecha_acreditacion .'<p>';
            echo '<p>Perfil Acreditado: ' . $certdata->perfil_acreditado . '<p>';
            echo '</div>';
            echo '</div>';
            echo '<div><img src="pix/certverified.png" border="0" align="center">';
            echo '<p class="verified">' . get_string('check_state', 'block_verificador_acreditaciones') . '</p>';
            echo '</div>';
            echo '</div>';
        }
    }

    echo $OUTPUT->box_end();
}
echo $OUTPUT->footer();
