<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * Verify Acreditacion block
 * --------------------------
 * Verify Acreditacion based on the unique codes displayed on issued acreditacions.
 * Full details of the issued Acreditacion is displayed including profile picture.
 * Mostly cosmetic changes to the original codes from Jean-Michel Védrine.
 * Original Autor & Copyright - Jean-Michel Védrine | 2014
 *
 * @copyright           2015 onwards Manieer Chhettri | Marie Curie, UK | <manieer@gmail.com>
 * @author              Augusto Villa | cunix.net
 * @package             block_verificador_acreditaciones 
 * @license             http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['title'] = 'Verificador de acreditaciones';
$string['pluginname'] = 'Verificador de acreditación';
$string['acreditacion'] = 'Verificación para la acreditación:';
$string['verifyacreditacion'] = 'Verificar Acreditación';
$string['verify_acreditacion:myaddinstance'] = 'Add a new Verify acreditacion block';
$string['notfound'] = 'El rut ingresado no se encuentra en nuestro sistema.<br> Por favor ingrese una acreditación existente';
$string['check_fail'] = 'El rut ingresado no se encuentra con acreditación vigente';
$string['check_fail_state'] = 'Estado de acreditacion: NO VIGENTE';
$string['check'] = 'El rut ingresado se encuentra con acreditación vigente';
$string['check_state'] = 'Estado de acreditacion: ACREDITADO';
$string['entercode'] = 'Ingrese Aquí su verificación:';
$string['validate'] = 'Verificar';
$string['dates'] = 'Datos:';

